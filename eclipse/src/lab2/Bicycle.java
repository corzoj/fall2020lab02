//student id: 1935301
//juan corzo
package lab2;

public class Bicycle {
	private String manufacturer;
	private int numberGears;
	private double maxSpeed;
	
	public Bicycle(String manu, int numbGear, double max) {
		manufacturer=manu;
		numberGears=numbGear;
		maxSpeed=max;
	}

	public String getManufacturer() {
		return manufacturer;
	}
	
	public int getNumberGears() {
		return numberGears;
	}
	
	public double getMaxSpeed() {
		return maxSpeed;
	}
	
	public String toString(){
		return ("manufacturer: "+getManufacturer()+" number of gears: "
				+getNumberGears()+" Max speed: "+getMaxSpeed());
	}
}
