//student id: 1935301
//juan corzo
package lab2;

public class BikeStore {
	public static void main(String[]args) {
		
		Bicycle bicycle1=new Bicycle("paco",12,50);
		Bicycle bicycle2=new Bicycle("marcos",15,60);
		Bicycle bicycle3=new Bicycle("juan",21,53);
		Bicycle bicycle4=new Bicycle("jesus",13,41);
		
		Bicycle bicycle[]=new Bicycle[4];
		bicycle[0]= bicycle1;
		bicycle[1]= bicycle2;
		bicycle[2]= bicycle3;
		bicycle[3]= bicycle4;
		
		for(int i = 0; i<4; i++ ) {
			System.out.println(bicycle[i]);
		}
	}

}
